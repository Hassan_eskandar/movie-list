## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Requirements

### create a virtual environment and and install requirements

```
pip install -r requirements.txt
```

## To run the program in local server use the following command

```
python manage.py runserver
Then go to http://127.0.0.1:8000/movies in your browser
```

## To test the application run

```
python manage.py test
```
