from django.test import TestCase
from django.urls import reverse
import requests

MOVIE_LIST_URL = reverse('movies')


class MovieViewTests(TestCase):
    """
    Test class for movie Views
    """

    def test_moive_list_view(self):
        """
        Test movie_list view
        """
        response = self.client.get(MOVIE_LIST_URL)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Studio Ghibli Films List')
        self.assertTemplateUsed(response, 'film_list.html')

    def test_films_api_endpoints(self):
        """
        Test movies and peoples api
        """
        response = requests.get("https://ghibliapi.herokuapp.com/films")
        self.assertEqual(response.status_code, 200)

    def test_peoples_api_endpoints(self):
        """
        Test movies and peoples api
        """
        response = requests.get("https://ghibliapi.herokuapp.com/people")
        self.assertEqual(response.status_code, 200)
