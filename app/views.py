from django.shortcuts import render
from django.views.generic import View
import requests


class HomeView(View):
    """
    Home View For Handling all requests at /movies endpoint
    """

    def get(self, request, *args, **kwargs):
        """
        Get method to handle all HTTP GET request from client
        """

        # Check if fetched api data is already on cache
        is_cached = (
            'film_data' in request.session and 'people_data' in request.session)

        # If data is not on cache, fetch new api data
        if not is_cached:
            """
            We will use requests library to create http request from server.
            """
            response = requests.get(
                'https://ghibliapi.herokuapp.com/films?limit=250')

            # convert all films json data into python objects and assign it to session
            request.session['film_data'] = response.json()

            # Using requests library to fech people api endpoint
            response = requests.get(
                'https://ghibliapi.herokuapp.com/people?limit=250')

            # convert all people json data into python objects and assign it to session
            request.session['people_data'] = response.json()

            # Set session expiry to 60 seconds
            request.session.set_expiry(60)

        # Create context object
        context = {
            'film_data': request.session['film_data'],
            'people_data': request.session['people_data']

        }

        # retrun and render html and context data to the client
        return render(request, 'film_list.html', context)
