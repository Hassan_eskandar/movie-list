from django.urls import path
from .views import HomeView

urlpatterns = [
    path('movies/', HomeView.as_view(), name="movies")
]
